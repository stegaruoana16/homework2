import React from 'react';
import axios from 'axios';

export class AddProduct extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.state.productName = "";
        this.state.price = 0;
    }
    handleChangeProductName = (event) => {
        this.setState({
            productName: event.target.value
        })
    
    handleChangeProductPrice = (event) => {
        this.setState({
            price: event.target.price
        })
    }
    addItem = () => {
        let product = {
            productName: this.state.productName,
            price: this.state.price
        }
        axios.post('https://product-api//server.js//add', product).then((res) => {
            if (res.state === 200) {
                this.props.itemAdd(product)
            }
        }).catch((err) => {
            console.log(err)
        })
    }

    render() {
        return (
            <div>
                <h1>Produs</h1>
                <input type="text" placeholder="Name"
                    onChange={this.handleChangeProductName}
                    value={this.state.productName}/>
                <input type="number" value={this.state.price}
                    onChange={this.handleChangeProductPrice}/>
                <button onClick={this.addItem}>Produs</button>
            </div>
            );
    }

}
