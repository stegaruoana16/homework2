const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
})

app.post('/add', (req, res) => {
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});

app.delete('/delete', (req, res) => {

    if (req.body.productName) {
        products.forEach(element => {
            if (element.productName == req.body.productName) {
                const id = element.id;
                products.splice(id, 1); 
                res.status(200).send(products);
            }
        })
    }
    else {
        res.status(500).send('Error');
    }
});

app.put('/update/:id', (req, res) => {
    const id = req.params.id;
    products.forEach((product) => {
      if (id == product.id){
            product.productName = req.body.productName;
            product.price = req.body.price;
            res.status(200).send(` ${product.id} modificat`);
      }
   });
});



app.listen(8080, () => {
    console.log('Server started on port 8080...');
});